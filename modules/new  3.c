case WRITE: {
      cout<<"write ";
      if(state == ESTABLISHED) {
        cout<<"established"<<endl;
        if(cs->state.SendBuffer.GetSize()+req.data.GetSize() > 
           cs->state.TCP_BUFFER_SIZE) {//send buffer dont have enough space
          reply.type = STATUS;
          reply.connection = req.connection;
          reply.bytes = 0;
          reply.error = EBUF_SPACE;
          MinetSend(sock, reply);
        }
        else {//enough space in sendbuffer
          cs->state.SendBuffer.AddBack(req.data);//append new data
        }

        //flow control, wouldn't send data overflow other conns
        if(cs->state.last_sent - cs->state.last_acked < cs->state.rwnd) {
          if(cs->state.last_sent == cs->state.last_acked) {//no data on transition
            //activate timer
            cs->bTmrActive = true;
            cs->timeout = Time()+5;//expire in 5 sec
            cs->state.tmrTries = 3;//retransmit only 3 times
          }
          unsigned int totalsend = 0;//total data send this time
          Buffer buffer = cs->state.SendBuffer;//copy buffer
          //get rid of data we already send, what left is data we haven't send
          buffer.ExtractFront(cs->state.last_sent-cs->state.last_acked);  
          unsigned int totaleft = buffer.GetSize();
          
          //int kk =0 ;//testing purpose
          //maximum amount of data we can sent and not cause over flow
          //Note, N is updated to be always less than rwnd
          unsigned int MAX_SENT = cs->state.GetN()-(cs->state.last_sent-cs->state.last_acked)-
            TCP_MAXIMUM_SEGMENT_SIZE;
          //while we have data left to send and total data send does not exceed 
          //our window size
          printf("total left%d,%d, maxsent%d\n",totaleft,totalsend,MAX_SENT);
          while(totaleft!=0 && 
                totalsend < MAX_SENT) {
            unsigned int bytes = min(totaleft,TCP_MAXIMUM_SEGMENT_SIZE);//data we send this time
            cout<<totalsend<<", "<<bytes<<endl;
            outgoing_packet = buffer.Extract(0,bytes);
            //creat packet
            createIPPacket(outgoing_packet, *cs, bytes, F_PSHACK,true,
                           cs->state.GetLastAcked()+1+totalsend);
            //send ip packet
            //if(kk!=1) {//testing purpose
            MinetSend(mux, outgoing_packet);
            //}
            // kk++;
            cs->state.SetLastSent(cs->state.GetLastSent()+bytes);
            totalsend += bytes;
            //update totaleft
            totaleft -= bytes;
          }
        
          reply.type = STATUS;
          reply.connection = req.connection;
          reply.bytes = totalsend;
          reply.error = EOK;
          MinetSend(sock, reply);
        }
        
      }
      else {
        cout<<"invalid state"<<endl;
      }
      
    }
      break;