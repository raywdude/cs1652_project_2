// You will build this in project part B - this is merely a
// stub that does nothing but integrate into the stack

// For project parts A and B, an appropriate binary will be 
// copied over as part of the build process

//
//
// Matt note to self: maybe make a ConnectionManager class that contains the connection 
// and functions to modify the connection?
//

#include "tcpstate.h"
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


#include <iostream>

#include "Minet.h"

using namespace std;
/*
struct TCPState {
    // need to write this
    std::ostream & Print(std::ostream &os) const { 
	os << "TCPState()" ; 
	return os;
    }
};
*/
// KANGED. Its a good idea to do this but why do it differently?
static const int F_SYN = 1;
static const int F_ACK = 2;
static const int F_SYNACK = 3;
static const int F_PSHACK = 4;
static const int F_FIN = 5;
static const int F_FINACK = 6;
static const int F_RST = 7;

void ForgePacket(Packet &packet_tomake, ConnectionToStateMapping<TCPState> &theCTSM, int TCPHeaderType, int size_of_data, bool timeout_packet) {
	cerr << "===============Forging a packet!===============" << endl;
	unsigned char flags = 0;
	int packet_size = size_of_data + TCP_HEADER_BASE_LENGTH + IP_HEADER_BASE_LENGTH;
	IPHeader ipheader_new;
	TCPHeader tcpheader_new;
	
	// Forge the IP header
	ipheader_new.SetSourceIP(theCTSM.connection.src);
	ipheader_new.SetDestIP(theCTSM.connection.dest);
	ipheader_new.SetTotalLength(packet_size);
	ipheader_new.SetProtocol(IP_PROTO_TCP);
	packet_tomake.PushFrontHeader(ipheader_new);
	cerr << "\nipheader_new: \n" << ipheader_new << endl;
	
	
	// Forge the TCP header
	// Deal with the type of TCP header first
	switch (TCPHeaderType) {
		case F_SYN:
			SET_SYN(flags);
			cerr << "It is a F_SYN!" << endl;
			break;

		case F_ACK:
			SET_ACK(flags);
			cerr << "It is a F_ACK!" << endl;
			break;

		case F_SYNACK:
			SET_ACK(flags);
			SET_SYN(flags);
			cerr << "It is a F_SYNACK!" << endl;
			break;
		
		case F_PSHACK:
			SET_PSH(flags);
			SET_ACK(flags);
			break;

		case F_FIN:
			SET_FIN(flags);
			break;

		case F_FINACK:
			SET_FIN(flags);
			SET_ACK(flags);
			break;

		case F_RST:
			SET_RST(flags);
			break;
		
		default:
			break;
	}
	
	// Continue forging TCP header
	tcpheader_new.SetSourcePort(theCTSM.connection.srcport, packet_tomake);
	tcpheader_new.SetDestPort(theCTSM.connection.destport, packet_tomake);
	tcpheader_new.SetHeaderLen(TCP_HEADER_BASE_LENGTH, packet_tomake);
	tcpheader_new.SetFlags(flags, packet_tomake);
	cerr << "\ntcpheader_new: \n" << tcpheader_new << endl;
	tcpheader_new.SetAckNum(theCTSM.state.GetLastRecvd(), packet_tomake);
	tcpheader_new.SetWinSize(theCTSM.state.GetRwnd(), packet_tomake);
	tcpheader_new.SetUrgentPtr(0, packet_tomake);
	
	// Determine if it is a packet related to a timeout
	if (timeout_packet) { // If this is a packet responding to a timeout scenario
		tcpheader_new.SetSeqNum(theCTSM.state.GetLastSent()+1, packet_tomake);
	}
	else { // If this is NOT a packet responding to a timeout scenario
		tcpheader_new.SetSeqNum(theCTSM.state.GetLastAcked()+1, packet_tomake);
	}
	
	tcpheader_new.RecomputeChecksum(packet_tomake); // Use instead of SetChecksum to set to zero first
	
	packet_tomake.PushBackHeader(tcpheader_new);
	// Done forging the packet
	cerr << "===============End forging!===============\n" << endl;
}





void mux_handler(const MinetHandle &mux, const MinetHandle &sock, ConnectionList<TCPState> &clist) {
	cerr << "\n===============Mux Handler Starting===============\n" << endl;
	Packet packet_received;
	Buffer buffer;
	MinetReceive(mux, packet_received);
	Connection conn_tbd;
	
	Packet packet_tosend;
	TCPHeader tcpheader_recv;
	IPHeader ipheader_recv;
	
	unsigned char flags;
	unsigned int ack;
	unsigned int seq;
	SockRequestResponse repl, req;
	unsigned short total_size;
	unsigned char tcpheader_size;
	unsigned char ipheader_size;
	unsigned short window_size, isUrgentPacket;
  
	packet_received.ExtractHeaderFromPayload<TCPHeader>(TCPHeader::EstimateTCPHeaderLength(packet_received));
	tcpheader_recv = packet_received.FindHeader(Headers::TCPHeader);
	ipheader_recv = packet_received.FindHeader(Headers::IPHeader);
	
	bool checksum = tcpheader_recv.IsCorrectChecksum(packet_received);
	cerr << tcpheader_recv << endl;
	if (!checksum) {
		cerr << "Incorrect checksum!\n";
		return;
	}
	
	cerr << "\nipheader: \n" << ipheader_recv << endl;
	cerr << "\ntcpheader: \n" << tcpheader_recv << endl;
	// The following are needed for identifying the connection (tuple of 5 values)
	cerr << "Note: some values are reversed because that way its easier to craft a packet" << endl;
	ipheader_recv.GetDestIP(conn_tbd.src);
	ipheader_recv.GetSourceIP(conn_tbd.dest);
	ipheader_recv.GetProtocol(conn_tbd.protocol);
	tcpheader_recv.GetSourcePort(conn_tbd.destport);
	tcpheader_recv.GetDestPort(conn_tbd.srcport);
	cerr << "Printing the connection received:\n" << conn_tbd << endl;
	
	// Hard coded connections for part 2a
	
	
	cerr << "===============START HARD CODED===============\n" << endl;
	ConnectionToStateMapping<TCPState> CTSM;
	char dest[] = "192.168.42.10";
	
	// Server mode
	TCPState server(0, LISTEN, 5);
	Connection conn;
	conn.dest = conn_tbd.dest;
	conn.src = MyIPAddr();
	conn.protocol = IP_PROTO_TCP;
	conn.srcport = conn_tbd.srcport;
	conn.destport = conn_tbd.destport;
	CTSM.connection = conn;
	CTSM.state = server;
	clist.push_back(CTSM);
	
	cerr << clist << endl;
	// Client mode
	
	
	cerr << "===============END HARD CODED===============" << endl;
	// End hard coded section
	
	
	
	
	// Get the flags from the TCP header
	ipheader_recv.GetFlags(flags);
	cerr << "flags: " << flags << endl;
	
	tcpheader_recv.GetSeqNum(seq);
	tcpheader_recv.GetAckNum(ack);
	tcpheader_recv.GetFlags(flags);
	tcpheader_recv.GetWinSize(window_size);	
	tcpheader_recv.GetUrgentPtr(isUrgentPacket);
	tcpheader_recv.GetHeaderLen(tcpheader_size);
	//tcpheader_size<<=2;
	
	ipheader_recv.GetTotalLength(total_size);
	ipheader_recv.GetHeaderLength(ipheader_size);
	//ipheader_size<<=2;
	
	total_size = total_size - tcpheader_size - ipheader_size;
	cerr << "ipheader_recv len = " << total_size << endl;
	buffer = packet_received.GetPayload().ExtractFront(total_size);
	
	
	ConnectionList<TCPState>::iterator CL_iterator = clist.FindMatching(conn_tbd);
	
	unsigned int current_state; // This is the current state of the connection
	
	if (CL_iterator == clist.end()) {
		cerr << "Didn't find the connection in the list!" << endl;
	}
	current_state = CL_iterator->state.GetState();
	
	cerr << "current_state: " << current_state << endl;
	
	switch(current_state){
		case LISTEN:
			//
			cerr << "===============START CASE LISTEN===============\n" << endl;
			if (IS_SYN(flags)) {
				cerr << "===============START IS_SYN===============" << endl;
				// Update all the data in the CTSM
				CL_iterator->connection = conn_tbd; // Set conn_tbd to connection
				CL_iterator->state.SetState(SYN_RCVD);
				CL_iterator->state.last_acked = CL_iterator->state.last_sent-1;
				CL_iterator->state.SetLastRecvd(seq + 1);
				CL_iterator->bTmrActive = true; // Timeout set
				CL_iterator->timeout=Time() + 5; // Set to 5 seconds
				cerr << "\nseq: " << seq << " and ack: " << ack << endl;
				
				// Forge the SYNACK packet
				ForgePacket (packet_tosend, *CL_iterator, F_SYNACK, 0, false);
				MinetSend(mux, packet_tosend);
				
				cerr << "===============END IS_SYN===============" << endl;
			}
			
			cerr << "===============END CASE LISTEN===============\n" << endl;
			break;
		case SYN_RCVD:
			// If you are in this state, you should have received the SYN packet
			// and thus sent a SYNACK and be expecting an ACK back
			cerr << "===============START CASE SYN_RCVD===============\n" << endl;
			CL_iterator->state.SetState(ESTABLISHED);
			cerr << "Established the connection!" << endl;
			CL_iterator->state.SetLastAcked(ack);
			CL_iterator->state.SetSendRwnd(window_size); 
			CL_iterator->bTmrActive = false;
			
			cerr << "===============END CASE SYN_RCVD===============\n" << endl;
			//where your seq is seq and ACK is their Seq+1
			break;
		case SYN_SENT:
			//Receive SYN ACK, go to ESTABLISHED, send Ack of
			//their SEQ+1 (could also send data)
			cerr << "CASE SYN_SENT" << endl;
			//completeHandshake(p);
			break;
		case ESTABLISHED:
			//TODO:Do normal transactions from packet info
			break;
		case FIN_WAIT1:
			//TODO: Receive ack, send nothing->FIN_WAIT_2
			break;
		case FIN_WAIT2:
			//TODO: Receive FIN, send ACK->TIME_WAIT
			break;
		case TIME_WAIT:
			//Wait 30 seconds...(so...nothing?)
			break;
		case CLOSE_WAIT:
			//Send FIN after just receiving FIN (also send ACK)
			break;
		case LAST_ACK:
			//Receive ack, DO NOTHING! LEAVE NOW!!!!
			break;
	
		return;
	}
	cerr << "\n===============Mux Handler Ending===============\n" << endl;
}

int main(int argc, char * argv[]) {
    MinetHandle mux;
    MinetHandle sock;
    
    ConnectionList<TCPState> clist;
	
// Passive/Active open constructor FOR REFERENCE FROM TCPSTATE.CC
//TCPState::TCPState(unsigned int initialSequenceNum, unsigned int state, unsigned int timertries)

	
	
	
	
	
    MinetInit(MINET_TCP_MODULE);

    mux = MinetIsModuleInConfig(MINET_IP_MUX) ?  
	MinetConnect(MINET_IP_MUX) : 
	MINET_NOHANDLE;
    
    sock = MinetIsModuleInConfig(MINET_SOCK_MODULE) ? 
	MinetAccept(MINET_SOCK_MODULE) : 
	MINET_NOHANDLE;

    if ( (mux == MINET_NOHANDLE) && 
	 (MinetIsModuleInConfig(MINET_IP_MUX)) ) {

	MinetSendToMonitor(MinetMonitoringEvent("Can't connect to ip_mux"));

	return -1;
    }

    if ( (sock == MINET_NOHANDLE) && 
	 (MinetIsModuleInConfig(MINET_SOCK_MODULE)) ) {

	MinetSendToMonitor(MinetMonitoringEvent("Can't accept from sock_module"));

	return -1;
    }
    
    cerr << "tcp_module STUB VERSION handling tcp traffic.......\n";

    MinetSendToMonitor(MinetMonitoringEvent("tcp_module STUB VERSION handling tcp traffic........"));

    MinetEvent event;
    double timeout = 1;

    while (MinetGetNextEvent(event, timeout) == 0) {

	if ((event.eventtype == MinetEvent::Dataflow) && 
	    (event.direction == MinetEvent::IN)) {
	
	    if (event.handle == mux) {
		// ip packet has arrived!
			//MinetSendToMonitor(MinetMonitoringEvent("tcp_module ip packet has arrived!"));
			//cerr << "tcp_module ip packet has arrived!\n";
			
			mux_handler(mux, sock, clist);
			
	    }

	    if (event.handle == sock) {
		// socket request or response has arrived
			MinetSendToMonitor(MinetMonitoringEvent("tcp_module socket request or response has arrived"));
			cerr << "tcp_module socket request or response has arrived\n";
	    }
	}

	if (event.eventtype == MinetEvent::Timeout) {
	    // timeout ! probably need to resend some packets
	}

    }

    MinetDeinit();

    return 0;
}
